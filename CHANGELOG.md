# Changelog

## 3.2.0 - 2025-11-25

### Security

- spip-team/securite#4853 Interdire par défaut l’évaluation de code PHP (qui ne devrait plus être nécessaire) lors de la prévisualisation du porte plume
- spip-team/securite#4853 Limiter par défaut l’usage de la prévisualisation dans l’espace public aux administrateurs

### Added

- #4835 `spip_barre.js` (originellement sur le dépôt `prive`) est désomais nommé `barre_inserer.js` et chargé via `porte_plume_inserer_head`
- spip-team/securite#4853 Autorisation `autoriser_modelesphp_previsualiser_dist` (retourne `false` par défaut)
- Installable en tant que package Composer

### Fixed

- spip-team/security#4843 Améliorer la prévisu lors de l’utilisation de certains modèles

### Changed

- spip-team/securite#4853 L’autorisation `autoriser_porteplume_previsualiser_dist` est limitée aux admins dans l’espace public.
- Compatible SPIP 5.0.0-dev

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.
