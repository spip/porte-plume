<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/barreoutils?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// B
	'barre_a_accent_grave' => 'Manisika À',
	'barre_adresse' => 'Adiresy',
	'barre_aide' => 'Mampiasa hitsin-dalana an-tarehin-tsotra hampisongadina ny fanehoana',
	'barre_alignerdroite' => '[/Mandahatra amin’ny havanana/] ny andiam-pehezanteny',
	'barre_ancres' => 'Fitantanana ny vatofantsika',
	'barre_barre' => 'Manipika ny soratra',
	'barre_bulle' => 'Bulle fanampiana',
	'barre_c_cedille_maj' => 'Manisika Ç',
	'barre_cadre' => 'Atao ao anaty &lt;cadre&gt;zone de saisie de texte&lt;/cadre&gt;',
	'barre_caracteres' => 'Tarehin-tsoratra manokana',
	'barre_centrer' => '[|Mandahatra eo afovoany|] ny andiam-pehezanteny',
	'barre_chercher' => 'Karohana Soloina',
	'barre_clean' => 'Esorina amin’ny kaody ny marika HTML rehetra',
	'barre_code' => 'Arafitra ny fanehoana ho &lt;code&gt;kaody informatika&lt;/code&gt;',
	'barre_desindenter' => 'Esorina ny fiatahan’ny lisitra',
	'barre_e_accent_aigu' => 'Manisika É',
	'barre_e_accent_grave' => 'Manisika È',
	'barre_ea' => 'Manisika æ',
	'barre_ea_maj' => 'Manisika Æ',
	'barre_encadrer' => '[(Hodidinina)] ny andiam-pehezanteny',
	'barre_eo' => 'Manisika œ',
	'barre_eo_maj' => 'Manisika Œ',
	'barre_euro' => 'Manisika ny marika famantarana €',
	'barre_exposant' => 'Atao &lt;sup&gt;mihantona&lt;/sup&gt; ny soratra',
	'barre_formatages_speciaux' => 'Fanehoana manokana',
	'barre_galerie' => 'Sokafana ny fanangonana',
	'barre_gestion_anc_bulle' => 'Bulle fanampiana amin’ny vatofantsika',
	'barre_gestion_anc_caption' => 'Fitantanana ny vatofantsika',
	'barre_gestion_anc_cible' => 'Vatofantsika kinendry',
	'barre_gestion_anc_inserer' => 'Avadika ho vatofantsika',
	'barre_gestion_anc_nom' => 'Anaran’ny vatofantsika',
	'barre_gestion_anc_pointer' => 'Manondro mankany amina vatofantsika',
	'barre_gestion_caption' => 'Maribolana sy Fintina',
	'barre_gestion_colonne' => 'Isan’ny tsanganana',
	'barre_gestion_cr_casse' => 'Hajaina ny kasy',
	'barre_gestion_cr_changercasse' => 'Soloina ny kasy',
	'barre_gestion_cr_changercassemajuscules' => 'Atao sora-baventy',
	'barre_gestion_cr_changercasseminuscules' => 'Atao sora-madinika',
	'barre_gestion_cr_chercher' => 'Karohana',
	'barre_gestion_cr_entier' => 'Teny manontolo',
	'barre_gestion_cr_remplacer' => 'Soloina',
	'barre_gestion_cr_tout' => 'Soloina avokoa',
	'barre_gestion_entete' => 'Lohan’ny pejy',
	'barre_gestion_ligne' => 'Isan’ny andalana',
	'barre_gestion_taille' => 'Habe raikitra',
	'barre_glossaire' => 'Fidirana amin’ny [?glossaire] (Wikipedia)',
	'barre_gras' => 'Atao {{sora-matavy}}',
	'barre_guillemets' => 'Fonosina « farango sosona »',
	'barre_guillemets_simples' => 'Fonosina “farango sosona ambaratonga faharoa”',
	'barre_indenter' => 'Ahataka ny lisitra',
	'barre_inserer_cadre' => 'Manisika kaody voarafitra mialoha (```)',
	'barre_inserer_caracteres' => 'Manisika tarehin-tsoratra voafaritra',
	'barre_inserer_code' => 'Manisika kaody informatika (`)',
	'barre_intertitre' => 'Avadika ho {{{zana-dohateny}}}',
	'barre_italic' => 'Atao {sora-mandry}',
	'barre_langue' => 'Fiteny nafohezina',
	'barre_lien' => 'Avadika ho [rohy->http://...]',
	'barre_lien_externe' => 'Rohy ivelany',
	'barre_lien_input' => 'Atoroy ny adiresin’ny rohy (azonao atao ny manondro adiresy Internet amin’ny endrika http://www.monsite.com, adiresy mailaka, na ny laharan’ny lahatsoratra ato amin’ny site.',
	'barre_liste_ol' => 'Avadika ho lisitra voalahatra',
	'barre_liste_ul' => 'Avadika ho lisitra',
	'barre_lorem_ipsum' => 'Manisika andiam-pehezanteny tsy misy heviny',
	'barre_lorem_ipsum_3' => 'Manisika andiam-pehezanteny telo tsy misy heviny',
	'barre_miseenevidence' => 'Atao [*misongadina*] ny soratra',
	'barre_note' => 'Avadika ho [[Fanamarihana ambany pejy]]',
	'barre_paragraphe' => 'Mamorona andalam-pehezanteny',
	'barre_petitescapitales' => 'Avadika ho &lt;sc&gt;sora-baventy madinika&lt;/sc&gt; ny soratra',
	'barre_poesie' => 'Arafitra ho toy ny &lt;poesie&gt;poezia&lt;/poesie&gt;',
	'barre_preview' => 'Fanehoana fitsirihana mialoha',
	'barre_quote' => '<quote>Manonona hafatra</quote>',
	'barre_stats' => 'Aseho ny antontan’isan’ny lahatsoratra',
	'barre_tableau' => 'Manisika/Manova (soritana mialoha) ny fafana',

	// C
	'config_info_enregistree' => 'Fandrafetana voatahiry',

	// E
	'editer' => 'Ovaina',
	'explication_barre_outils_public' => 'Ireo rindram-baiko CSS sy Javascript mahakasika ny vatam-pitaovana
	(extension Porte Plume) dia misondrotra eo amin’ny habaka talaky maso
	ka ahafahana mampiasa ny vatam-pitaovana amin’ny forums,
	sy ny pensily na ny plugins hafa, raha mamela izany
	ny fandrafetana azy avy.',
	'explication_barre_outils_public_2' => 'Afaka misafidy ny tsy hampisondrotra
	ireo rindram-baiko ireo ianao mba hanamaivana ny lanjan’ny ny pejy talaky maso.
	Noho izany, na toa inona na toa inona fandrafetana ny forums, ny pensily na ny plugin,
	dia tsy hisy vatam-pitaovana avy amin’ny Porte Plume mihitsy
	hiseho amin’ny naham-pony eo amin’ny habaka talaky maso.',

	// I
	'info_barre_outils_public' => 'Vatam-pitaovana talaky maso',
	'info_porte_plume_titre' => 'Fandrafetana ny vatam-pitaovana',

	// L
	'label_barre_outils_public_non' => 'Tsy asondrotra eo amin’ny habaka talaky maso ireo rindram-baiko ho an’ny vatam-pitaovana',
	'label_barre_outils_public_oui' => 'Asondrotra eo amin’ny habaka talaky maso ny rindram-baiko ho an’ny vatam-pitaovana',

	// V
	'voir' => 'Mijery',
];
